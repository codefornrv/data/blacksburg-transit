# Blacksburg Transit

Access to Blacksburg Transit data is setup from our database through a [PostgREST](https://postgrest.org) API endpoint. Please see their [documentation](https://postgrest.org/en/v5.2/api.html) for help in selecting and filtering rows and modifying output. If you have a specific request or need help, feel free to create an issue and we'll try to help as best we can!

## Ridership
The only table available at the moment is the `ridership` table, which can be accessed at `https://bt.api.codefornrv.org/ridership?limit=15`. This has many rows, so the limit on the link is just for safety sake and can be removed if you want more data.

There are 8 columns total:

| column_name | type | description |
| --- | --- | --- |
| vehiclename | varchar(10) | The identifier used to specify the bus vehicle |
| routename | varchar(50) | The Blacksburg Transit designated route name the bus is one |
| patternname | varchar(50) | The pattern the route is following, as routes can vary depending on time of day, day of the week, seasonality, etc |
| stopname | varchar(50) | The name of the stop |
| actualdeparttime | timestamp | The timestamp when the bus left the bus stop |
| boardcount | smallint | The number of passengers who got on to the bus |
| alightcount | smallint | The number of passengers who got off of the bus |
| totalcount | smallint | The number of passengers on the bus as reported by the system |

### Time range
Our data currently ranges from January 3, 2012 to May 30, 2015. We are currently trying to get access to more recent data.

### Data accuracy
This data comes from automatic sensors on the buses. Blacksburg Transit has said that the boarding count is more accurate than the alight count, and that you will see total count often increase inaccurately because of this, leading to buses that have a total count higher than their actual physical capacity.
